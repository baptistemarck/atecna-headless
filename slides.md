---
# try also 'default' to start simple
theme: default
# random image from a curated Unsplash collection by Anthony
# like them? see https://unsplash.com/collections/94734566/slidev
background: https://media2.giphy.com/media/xsF1FSDbjguis/giphy.gif
# apply any windi css classes to the current slide
class: 'text-left text-xs'
# https://sli.dev/custom/highlighters.html
highlighter: shiki
# show line numbers in code blocks
lineNumbers: false
# some information about the slides, markdown enabled
info: |
    ## Slidev Starter Template
    Presentation slides for developers.

    Learn more at [Sli.dev](https://sli.dev)
# persist drawings in exports and build
drawings:
    persist: false
---

# Le futur est headless.

<hr/>

Et le futur c'est déjà maintenant.

---
class: 'text-left text-xs flex flex-col align-center justify-center'
layout: image-right
image: https://64.media.tumblr.com/d5e6d6deb46e46dd46573f2959d3d462/b0d7bbe4a49a8118-98/s540x810/e2d70b50ccec257f99e0e3661d6805a347dfc8c6.gifv
---

<div>

# C'est quoi le headless ?

<hr/>

Un CMS headless est un système de gestion de contenu qui fournit un moyen de créer du contenu, mais au lieu d'avoir votre contenu couplé à une sortie particulière (comme le rendu de page Web), il fournit votre contenu sous forme de données via une API.

<v-clicks>

- 🎃 **Head** - le contenu rendu.
- 🐎 **Body** - où le contenu est stocké et créé.

</v-clicks>
</div>

<style>
.slidev-layout {
  h1 {
    @apply text-xl;
  }

  li {
    p {
      margin: 0;
    }
  }
}
</style>

---
class: 'text-left text-xs'
---

# Headless CMS vs Traditional CMS

<hr/>

<div class="grid grid-cols-3 gap-20 pt-8">
  <div>
    <img v-click at="1" src="/coupled.png" />
    <ul class="text-xs p-6">
      <li v-click at="2">Traditional CMS</li>
      <li v-click at="3">Self host</li>
      <li v-click at="4">Websites</li>
    </ul>
  </div>
  <div>
    <img v-click at="5" src="/decoupled.png" />
    <ul class="text-xs p-6">
      <li v-click at="6">Headless CMS</li>
      <li v-click at="7">Saas or Hosted</li>
      <li v-click at="8">Web oriented</li>
    </ul>
  </div>
  <div>
    <img v-click at="9" src="/semi-decoupled.png" />
    <ul class="text-xs p-6">
      <li v-click at="10">Structured Content Platform</li>
      <li v-click at="11">Fully decoupled + customizable</li>
      <li v-click at="12">SaaS</li>
      <li v-click at="13">Any channel</li>
      <li v-click at="14">Real-time collaboration</li>
      <li v-click at="15">API native</li>
    </ul>
  </div>
</div>

---
class: 'text-left text-xs'
---

# Comparaison

<hr class="mb-8"/>

|                                | **TRADITIONAL CMS** | **HEADLESS CMS**     |
|--------------------------------|---------------------|----------------------|
| **Architecture**               | One-to-one          | One-to-many          |
| **Extensibility**              | Monolithic          | Modular              |
| **Device compatibility**       | Limited             | Responsive by design |
| **Integration and deployment** | Punctuated          | Continuous           |
| **Engineering**                | DIY                 | Managed              |
| **Developer experience**       | Legacy              | Contemporary         |
| **Speed**                      | Higher load times   | Lower load times     |
| **Iteration**                  | Long cycles         | Rapid development    |

---
layout: center
class: 'text-left text-xs'
---

# Les avantages du Headless CMS

<hr/>

<div class="grid grid-cols-2 gap-20 mt-8">
<div>
<div v-click at="1" text="sm">

## 🔒 Sécurité renforcée

Étant donné que le **body** est séparé du **head**, il s'agit d'une zone d'attaque plus petite.

</div>

<div v-click at="2" text="sm">

## ⏱️ Expériences d'édition plus rapides

Les architectures CMS traditionnelles doivent consacrer des ressources à l'édition et au rendu du contenu. Un CMS headless a un avantage sur les alternatives traditionnelles car il n'a pas à gérer le rendu. C'est laissé à d'autres parties plus spécialisées de votre pile.

</div>
<div v-click at="3" text="sm">

## 🥰 Flexibilité du développeur

Étant donné que le contenu headless est diffusé via des API, les développeurs peuvent choisir leurs propres outils frontend. Si vous préférez travailler avec Javascript au lieu de PHP ou Ruby, vous pouvez le faire. Vous pouvez également échanger des parties de votre pile ou passer d'un framework à un autre sans affecter le CMS.

</div>
</div>
<div>
<div v-click at="4" text="sm">

## 🎌 Gérer le contenu pour plus de canaux

Le contenu véritablement headless n'est pas lié à une seule problématique de présentation (par exemple, un site Web), il peut donc trouver un public sur plusieurs canaux. Vous pouvez utiliser un CMS headless pour gérer le contenu des applications et des sites Web. Vous pouvez même gérer votre contenu interne/admin au même endroit et en tirer plus de valeur de cette façon.

</div>

<div v-click at="5" text="sm">

## ✨ Facilite la scalability

Le headless vous permet de gérer votre contenu à partir d'une source unique de vérité, de changer d'outils de développement à tout moment et de bénéficier de l'envoi de votre contenu vers un hébergement cloud haute performance et de créer des services comme Vercel et Netlify.

</div>
</div>
</div>

<style>
.slidev-layout {
  h2 {
    @apply text-sm;
  }

  p {
    @apply text-xs;
    @apply mt-2;
  }
}
</style>

---
layout: center
class: 'text-left text-xs'
---

# Use cases

<hr/>

<div class="grid grid-cols-3 gap-20 mt-8">
<div v-click at="1" text="sm">

## <carbon-application-web /> Sites Web et applications Web
Ils sont un choix populaire dans les sites « Jamstack » très performants et fonctionnent avec des générateurs de sites statiques comme Gatsby, 11ty et Next. Ils peuvent également être utilisés pour les applications Web et sont populaires avec les frameworks Javascript modernes tels que React, Vue.js, Svelte et Angular.

</div>
<div v-click at="2" text="sm">

## <carbon-product /> Produits et services
Un CMS headless bien conçu ne sera pas orienté vers une structure de contenu basée sur des pages (qui devrait être facile à créer, mais facultative). Si tel est le cas, vous pouvez gérer le contenu de n'importe quel produit ou service : assistants vocaux, kiosques numériques, imprimés, sites Web, le tout depuis le même endroit.

</div>
<div v-click at="3" text="sm">

## <carbon-delivery /> E-commerce
Certains CMS headless sont suffisamment flexibles pour être provisionnés en tant que back-end de e-commerce. Vous pouvez également intégrer du contenu headless aux plates-formes e-commerce existantes et aux systèmes de gestion des stocks de produits tels que Shopify et SAP Hybris.

</div>
</div>

<style>
.slidev-layout {
  h2 {
    @apply text-sm;
  }

  p {
    @apply text-xs;
    @apply mt-2;
  }
}
</style>

---
layout: center
---

# Headless API Types

<hr/>

<div class="grid grid-cols-2 gap-20 mt-8">
<div text="sm">

```json {all|12-15}
{
    "_createdAt": "2020-04-25T11:21:25Z",
    "_id": "XXXXXX",
    "_type": "company",
    "logo": {
        "_type": "image",
        "asset": {
            "_ref": "image-2bfd3a-atecna-logo-svg",
            "_type": "reference"
        }
    },
    "slug": {
        "_type": "slug",
        "current": "atecna"
    },
    "title": "Atecna"
}
```

</div>

<div>
<div v-click at="3" text="sm">

## <carbon-api-1 /> REST API

Les API peuvent être plus ou moins flexibles lorsqu'il s'agit de créer des produits dessus. Traditionnellement, les CMS headless  proposaient des API REST. Ils structurent le contenu derrière plusieurs URL (par exemple, /posts /authors /images). Les développeurs doivent assembler plusieurs requêtes avec les identifiants des différents types de contenu. Les API REST peuvent être idéales pour les structures de données simples, mais leur utilisation a tendance à prendre du temps lorsque vos modèles de contenu sont plus complexes. Il est également plus laborieux de modifier ou d'adapter ces API à différentes utilisations.

</div>
<div v-click at="4" text="sm">

## <logos-graphql /> GraphQL

Facebook a développé GraphQL en réponse à la convention REST moins flexible. Il vous permet d'interroger l'API avec les champs et les relations dont vous avez besoin pour chaque occasion, presque comme ce que vous pouvez faire directement avec une base de données. Il est également plus facile d'implémenter de nouveaux champs et types de contenu à mesure que votre système évolue.

</div>
</div>
</div>

<style>
.slidev-layout {
  h2 {
    @apply text-sm;
  }

  p {
    @apply text-xs;
    @apply mt-2;
  }
}
</style>

---
class: 'text-left text-md'
---

# Architecture Headless

<hr />

<div class="grid grid-cols-1 gap-20 mt-8">

```mermaid {theme: 'dark', scale: 0.8}
flowchart LR
subgraph SHIELD
C[Headless CMS] <--> BACK
PIM[PIM] --> BACK{Back}
DAM[DAM] --> BACK{Back}
OTHER[...] --> BACK{Back}
end
BACK --GET--> HAPI
HAPI[Headless API] --> API{API}
EAPI[External API] --> API{API}
API --> FE{Front-end}
FE --> VUE[Vue]
FE --> REACT[React]
FE --> SVELTE[Svelte]
FE --> OTHERFR[...]
STORYBOOK[Storybook] --> FE
PRIVATE[Private package] --> FE
VUE --> CI{CI/CD}
REACT --> CI{CI/CD}
SVELTE --> CI{CI/CD}
OTHERFR --> CI{CI/CD}
CI --> WEB[Web]
CI --> APP[App]
CI --> O[...]
```

<style>
.mermaid:deep(svg) {
  width: 100%
}
</style>

</div>

---
layout: center
class: 'text-left text-md'
---

# Comment choisir le bon CMS headless ?

<hr class="mb-8" />

<v-clicks>

- Puis-je créer les types de structures de contenu dont j'ai vraiment besoin ?
- Est-ce que je veux ou dois-je m'occuper moi-même de l'hébergement et de la maintenance du contenu ?
- Mon contenu sera-t-il stocké de manière sécurisée et conforme à la confidentialité ?
- L'édition et la collaboration en temps réel amélioreraient-elles mes flux de travail ?
- Mon contenu en texte enrichi sera-t-il verrouillé en HTML ?
- Puis-je faire évoluer mes opérations de contenu sans tomber dans une falaise de prix ?
- Comment gère-t-il les fichiers et les ressources d'image ?

</v-clicks>

---
layout: center
class: 'text-left text-md'
---

# En resumé

<hr class="mb-8" />

Un CMS headless offre aux éditeurs une interface pour gérer facilement le contenu, tout en fournissant des API aux développeurs pour créer des applications, ce qui simplifie et accélère le stockage, l'édition et la publication de contenu. Ils diffèrent des CMS traditionnels et découplés car ils sont exclusifs à l'API et n'ont rien à voir avec le rendu du contenu.

---
layout: center
class: 'text-center text-md'
---

# The choice is yours

Merci

<style>
.slidev-layout {
  ::before {
    content: "";
    position: absolute;
    width: 200%;
    height: 200%;
    top: -50%;
    left: -50%;
    z-index: -1;
    background: url('/bg.png') 0 0 repeat;
    background-size: 25%;
    transform: rotate(30deg);
  }
}


</style>
